import json
import pickle
import numpy as np
import matplotlib.pyplot as plt
from os import listdir, mkdir, walk
from os.path import isdir, basename, normpath, dirname, isfile, join
from PIL import Image


def add_row_to_df(df, idx, values_list):
    df.loc[idx] = values_list


def remove_part_string(my_str, remove_from=1, remove_until=None, sep='_'):
    return sep.join(my_str.split(sep)[remove_from:remove_until])


def get_unique_values(df, col, verbose=False):
    unique = list(set(df[col].values.tolist()))
    if verbose:
        print('list has {} elements'.format(unique.__len__()))
    return unique


def filter_df_by_col_values(df, col, values, allowed=True, reset_index=True):
    if isinstance(values, list):
        mask = df[col].isin(values)
    elif isinstance(values, str):
        mask = df[col].str.contains(values)
    elif isinstance(values, int):
        mask = df[col] == values
    else:
        assert False, 'type {} not recognized'.format(type(values))
    if allowed:
        df = df[mask]  # stay with only allowed values
    else:
        df = df[~mask]
    if reset_index:
        df.reset_index(drop=True, inplace=True)
    return df


def get_batch(df, column, by_value, allowed=True, reset_index=True):
    if isinstance(by_value, list):
        mask = df[column].isin(by_value)
    else:
        mask = df[column] == by_value
    if allowed:
        batch = df[mask]  # stay with only allowed values
    else:
        batch = df[~mask]
    if reset_index:
        batch.reset_index(drop=True, inplace=True)
    return batch


def is_all_nan(df, col):
    return df[col].isnull().sum() == df.shape[0]


def load_dir_files(path_to_files, ext):
    return [file for file in listdir(path_to_files) if file.endswith('.{}'.format(ext))]


def get_files_names(path, sample_N=None):
    """
    Extract order ids from images names.
    :param path: path to image directory
    :return: list of order_ids corresponding to all images in the inputted directory
    """
    assert isdir(path), "Images path ({p}) is not valid!".format(p=path)
    file_names = []
    for (_, _, filenames) in walk(path):
        file_names.extend(filenames)
        break
    if sample_N is None:
        return file_names
    else:
        import random
        return random.sample(file_names, sample_N)


def get_list_of_all_files(dirName):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if isdir(fullPath):
            allFiles = allFiles + get_list_of_all_files(fullPath)
        else:
            allFiles.append(fullPath)

    return allFiles


def get_list_of_sub_dirs(path):
    return next(walk(path))[1]


def read_json(j_path):
    with open(j_path, 'r') as myfile:
        data = myfile.read()
        obj = json.loads(data)
    return obj


def write_json(j_objects, j_path):
    with open(j_path, 'w') as fp:
        json.dump(j_objects, fp, indent=4, sort_keys=True)


def remove_indices_from_list(my_list, indices):
    """
    E.D: remove indexes from my_list
    :param my_list: list of elements
    :param indices: list of indexes to be removed from :param my_list
    :return: my_list after removing the corresponding elements
    """
    for index in sorted(indices, reverse=True):
        del my_list[index]


def remove_indices_from_lists(my_lists, indices):
    """
    E.D: remove indexes from each list in :param my_lists
    :param my_lists: list of lists
    :param indices: list of indexes to be removed from each list at :param my_lists or integer for removing specific idx
    :return: my_lists after removing the corresponding elements from each list
    """
    for my_list in my_lists:
        if isinstance(indices, list):
            remove_indices_from_list(my_list, indices)
        else:
            del my_list[indices]


def find_lists_intersect(*args, verbose=True):
    inter = list(set(args[0]).intersection(*args))
    if verbose: print(inter.__len__())
    return inter


def list_subtraction(a, b):
    """
    remove b elements from a
    """
    return [item for item in a if item not in b]


def cast_list_elements(my_list, to_type):
    return [to_type(item) for item in my_list]


def list_to_text_file(txt_path, my_list):
    if '.txt' not in txt_path:
        txt_path += '.txt'
    # create a text file with all elements as rows
    with open(txt_path, "w") as text_file:
        for l in my_list:
            print(l, file=text_file)


def hot_encode_labels(label_set):
    return {x.lower(): n for n, x in enumerate(label_set)}


def text_file_to_list(file_path, split_by='\n', remove_elements=None):
    """
    convert a text file to list
    :param file_path: path to text file
    :param split_by: split text file by string (e.g., \n , . etc.)
    :param remove_elements: remove elements from resulted list
    :return: list hold the elements in the text file
    """
    if remove_elements is None:
        remove_elements = ['']
    my_file = open(file_path, 'r')
    barcodes = my_file.read().split(split_by)
    return list_subtraction(barcodes, remove_elements)


def lists_intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))


def clean_description(desc):
    desc = desc.replace(' ', '_')
    return ''.join(x for x in desc if validate_char(x))


def validate_char(x):
    if x == '_' or x.isdigit():
        return True
    elif x.isalpha():
        if ('a' <= x <= 'z') or ('A' <= x <= 'Z'):
            return True
    else:
        return False


def ser(my_object, file_path, verbose=True):
    """
    serialize an object
    :param my_object: the object to serialize
    :param file_path: path to save the serialized object
    :param verbose
    """
    if verbose:
        print('Serializing object to {}'.format(file_path))
    assure_dir(get_dir(file_path))
    with open(file_path, "wb") as f:
        pickle.dump(my_object, f, pickle.HIGHEST_PROTOCOL)


def deser(file_path, verbose=True):
    """
    deserialize object
    :param file_path: path to serialized object
    :return: deserialized object. i.e., python object
    """
    if isfile(file_path):
        if verbose:
            print('de-serializing object from {}'.format(file_path))
        with open(file_path, "rb") as f:
            dump = pickle.load(f)
        return dump
    else:
        print('file {f} for deserialization was not found'.format(f=file_path))
        return None


def assure_dir(dir_path):
    """
    in case dir not available - create it
    :param dir_path: path to some directory
    """
    if dir_path == '':
        return
    if not isdir(dir_path):
        print('creating dir ({})'.format(dir_path))
        if not isdir(get_dir(dir_path)):
            assure_dir(get_dir(dir_path))
        mkdir(dir_path)


def get_dir(f_path):
    """
    extract dir path from file path
    :param f_path: path to file
    :return: path to file's directory
    """
    return dirname(f_path)


def get_file_name(f_path):
    """
    extract file name from file path
    :param f_path: path to file
    :return: file name
    """
    return basename(normpath(f_path))




#############################
# new or modified functions #
#############################


def read_image(src_path):
    im = Image.open(src_path)
    return np.array(im)


def crop_image(im, y_keep=None, x_keep=None):
    """
    crop image on spatial dims
    Args:
        im: image numpy array of 3 dims
        y_keep: None if not to crop, or list of two integers to specify rows numbers (coords on y-axis) to keep
        x_keep: same as y_crop
    Returns: new crpped image

    """
    if y_keep is not None:
        im = im[int(y_keep[0]):int(y_keep[1]), :, ...]
    if x_keep is not None:
        im = im[:, int(x_keep[0]), int(x_keep[1]), ...]
    return im


def plot_series_hist(col, bins=15):
    """
    # use as follows to plot hist of specific df column:
    plot_hist(df['column_name'], bins=30)
    """
    col.hist(bins=bins)
    plt.show()
