# Copyright 2018 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

""" Offline data generation for the OFEK dataset."""
import random
import os
import cv2
import os.path as osp
import numpy as np
import pandas as pd
import xml.etree.ElementTree as ET
from absl import app
from tqdm import tqdm
from scipy.interpolate import interp1d
from scipy.ndimage.morphology import binary_dilation
from scipy.ndimage.measurements import label
from utils_elad import load_dir_files, get_list_of_sub_dirs, crop_image

SEQ_LENGTH = 3
WIDTH = 416
HEIGHT = 128
STEPSIZE = 1
INPUT_DIR = '/home/Elad/dev/datasets/Hamigdal14'
OUTPUT_DIR = '/home/Elad/dev/datasets/Hamigdal14_processed_no_stops_ang_velocity_0002_gapped_seq_5_crop_y_300'
CALIB_XML_TAGS = ['Fx', 'Fy', 'Px', 'Py']
INPUT_FILE_EXTENSION = 'pgm'
OUTPUT_FILE_EXTENSION = 'png'
GENERATE_DUMMY_MASKS = True
FILTER_BY_ODD_OR_EVEN = 'ODD'  # keeping bright ('EVEN'/group_id=0) or dark ('ODD'/group_id=1) frames
VAL_RATIO = 0.1
# IN_SEQUENCE_GAP_BETWEEN_FRAMES = [1, 5, 10, 20, 30]
IN_SEQUENCE_GAP_BETWEEN_FRAMES = [5]
SKIP_SEQUENCE_PROB = -1
ANGULAR_VELOCITY_CUTOFF = 0.0002
VERTICAL_CROP_IMAGE = [0, 300]  # None - do nothing, or list of size two integers for rows to keep coordinate

assert len(IN_SEQUENCE_GAP_BETWEEN_FRAMES) == 1, 'choose only one gap or extend "find high angular velocity"'


def get_camera_intrinsics(file_calib):
    assert osp.isfile(file_calib), 'calibration file was not found at {}'.format(file_calib)
    calib_camera = np.zeros(shape=(3, 3), dtype=np.float64)
    root = ET.parse(file_calib).getroot()
    for tag in CALIB_XML_TAGS:
        element = root.find(osp.join('Global', tag))
        val = element.text
        if tag == 'Fx':
            calib_camera[0, 0] = val
        elif tag == 'Fy':
            calib_camera[1, 1] = val
        elif tag == 'Px':
            calib_camera[0, 2] = val
        elif tag == 'Py':
            calib_camera[1, 2] = val
    return calib_camera


def filter_files(files, keep_movement_frames, movemvent_frames_labels):
    """
    filter files list
    Args:
        files: list of all dataset frame filenames in the format of: frame-000000XXXXXX.pgm
        keep_movement_frames: list hold frame number in which the robot is moving. we want to keep those frames

    Returns:
        files: same as the inputted "files" list, just after filtering irrelevant filenames
    """
    print('filter irrelevant frames..')
    # keep frames according to light/dark group
    if FILTER_BY_ODD_OR_EVEN is None:
        files = [file for file in files if 'seg' not in file]
    elif FILTER_BY_ODD_OR_EVEN == 'ODD':  # stay with darker images
        files = [file for file in files if 'seg' not in file and int(file.split('.')[0].split('frame-')[1]) % 2 == 1]
    elif FILTER_BY_ODD_OR_EVEN == 'EVEN':  # stay with lighter images
        files = [file for file in files if 'seg' not in file and int(file.split('.')[0].split('frame-')[1]) % 2 == 0]
    # keep frames (and corresponding labels) in which the robot is moving
    keep_files = []
    keep_labels = []
    for file in tqdm(files):
        frame_num = int(file.split('.')[0].split('frame-')[1])
        if frame_num not in keep_movement_frames:
            continue
        keep_files.append(file)
        frame_index = keep_movement_frames.index(frame_num)
        frame_label = movemvent_frames_labels[frame_index]
        keep_labels.append(frame_label)
    return keep_files, keep_labels


def get_cam1_interpolation_func(curr_dir):
    cam_meta_path = osp.join(curr_dir, 'meta', 'cam1.csv')
    cam_df = pd.read_csv(cam_meta_path)
    cam_frame_num = cam_df['frame_num']
    cam_timestamp = cam_df['timestamp_ns']
    return interp1d(cam_timestamp, cam_frame_num)


def get_valid_movement_frames(curr_dir):
    inter_time_to_frame = get_cam1_interpolation_func(curr_dir)
    robot_meta_path = osp.join(curr_dir, 'meta', 'superdroid_msgs.csv')
    robot_df = pd.read_csv(robot_meta_path)
    gross_num_frames_in_gap = IN_SEQUENCE_GAP_BETWEEN_FRAMES[0] * 2  # multiply by 2 due to two group ids
    binary_structure = np.ones(gross_num_frames_in_gap)
    # find high angular velocity: (diff_t1 - diff_t2)/delta_t
    robot_df['encoders_diff'] = robot_df['encoder_left'] - robot_df['encoder_right']
    robot_df['encoders_diff_delta_t'] = robot_df['encoders_diff'] - robot_df['encoders_diff'].shift(
        periods=gross_num_frames_in_gap)
    robot_df['delta_t'] = robot_df['timestamp_left'] - robot_df['timestamp_left'].shift(
        periods=gross_num_frames_in_gap)
    robot_df['angular_velocity'] = (robot_df['encoders_diff_delta_t'] / robot_df['delta_t']).abs()
    robot_df['is_high_ang_velocity'] = robot_df['angular_velocity'] > ANGULAR_VELOCITY_CUTOFF
    robot_df['is_high_ang_velocity'] = binary_dilation(robot_df['is_high_ang_velocity'], binary_structure)
    # find no movements
    robot_df['no_movement_left'] = robot_df['encoder_left'].eq(robot_df['encoder_left'].shift())
    robot_df['no_movement_right'] = robot_df['encoder_right'].eq(robot_df['encoder_right'].shift())
    # extend no movements
    robot_df['no_movement_left'] = binary_dilation(robot_df['no_movement_left'], binary_structure)
    robot_df['no_movement_right'] = binary_dilation(robot_df['no_movement_right'], binary_structure)
    robot_df['no_movement_both'] = binary_dilation(robot_df['no_movement_left'] & robot_df['no_movement_right'],
                                                   binary_structure)
    # find consistent movement groups
    robot_df['is_movement'] = ~(robot_df['no_movement_both'] | robot_df['is_high_ang_velocity'])
    robot_df['movement_labels'], _ = label(robot_df['is_movement'])
    # remove movement with less than 25 frames
    counts = robot_df['movement_labels'].value_counts()
    irrelevant_labels = counts[counts < 25].index.values
    irrelevant_mask = robot_df['movement_labels'].isin(irrelevant_labels)
    robot_df.loc[irrelevant_mask, 'movement_labels'] = 0
    # keep only relevant rows
    movement_df = robot_df[robot_df['movement_labels'] != 0][['host_timestamp', 'movement_labels']]
    # interpolate: robot_timestamp => camera timestamp => camera frame num
    movement_df['frame_num'] = inter_time_to_frame(movement_df['host_timestamp']).round().astype(np.int)
    movement_df.drop_duplicates(subset='frame_num', inplace=True)
    return movement_df['frame_num'].tolist(), movement_df['movement_labels'].tolist()


def main(_):
    random.seed(123)
    assert not osp.isdir(OUTPUT_DIR), 'output dir ({}) already exist'.format(OUTPUT_DIR)
    if not osp.isdir(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    for d in get_list_of_sub_dirs(INPUT_DIR):  # loop over all sub-directories (== sub data-sets)
        if not osp.isdir(osp.join(OUTPUT_DIR, d)):
            os.mkdir(osp.join(OUTPUT_DIR, d))
        curr_subdir = osp.join(INPUT_DIR, d)
        print('preprocess dataset: {}'.format(curr_subdir))
        # get camera intrinsics
        file_calibration = osp.join(curr_subdir, 'meta', 'CameraCalibration.xml')
        calib_camera = get_camera_intrinsics(file_calibration)
        # get all images/frames and filter them
        files = load_dir_files(curr_subdir, ext=INPUT_FILE_EXTENSION)
        files = sorted(files)
        movement_frames, movement_labels = get_valid_movement_frames(curr_subdir)
        files, labels = filter_files(files, movement_frames, movement_labels)
        # continue here. make sure that seq is composed of the save label group
        if GENERATE_DUMMY_MASKS:
            dummy_mask = np.zeros(shape=(HEIGHT, WIDTH * SEQ_LENGTH, 3), dtype=np.uint8)
        num_frames = len(files)
        ct = 1
        # group frames into sequences
        print('generate sequences..')
        for i in tqdm(range(SEQ_LENGTH, num_frames + 1, STEPSIZE)):  # iterate over all frames to create sequences
            curr_label = labels[i - SEQ_LENGTH]
            for gap_between_frames in IN_SEQUENCE_GAP_BETWEEN_FRAMES:
                if random.uniform(0, 1) < SKIP_SEQUENCE_PROB:  # skip random sequences due to data redundancy
                    continue
                imgnum = str(ct).zfill(12)
                if osp.exists(osp.join(OUTPUT_DIR, d, '{}.{}'.format(imgnum, OUTPUT_FILE_EXTENSION))):
                    ct += 1
                    print('sequence {} already generated ({})..'.format(
                        ct, osp.join(OUTPUT_DIR, d, '{}.{}'.format(imgnum, OUTPUT_FILE_EXTENSION))))
                    continue
                big_img = np.zeros(shape=(HEIGHT, WIDTH * SEQ_LENGTH, 3))
                frames_paths = []  # to enable seq to frames mapping
                wct = 0  # inner-sequence counter
                skip_sequence = False
                # Collect frames for this sample/sequence
                for j in range(i - SEQ_LENGTH, i - SEQ_LENGTH + gap_between_frames * SEQ_LENGTH, gap_between_frames):
                    if j >= num_frames - 1 or curr_label != labels[j]:
                        # make sure that all frames in seq has similar "movement group"
                        skip_sequence = True
                        continue
                    frame_path = osp.join(curr_subdir, files[j])
                    assert osp.isfile(frame_path), 'image of seq {} was not found at {}'.format(ct, frame_path)
                    img = cv2.imread(frame_path)
                    if VERTICAL_CROP_IMAGE is not None:
                        img = crop_image(img, VERTICAL_CROP_IMAGE)
                    assert img is not None, 'image (frame) {} is not valid'.format(frame_path)
                    frames_paths.append(osp.join(d, files[j]))
                    ORIGINAL_HEIGHT, ORIGINAL_WIDTH, _ = img.shape
                    zoom_x = WIDTH / ORIGINAL_WIDTH
                    zoom_y = HEIGHT / ORIGINAL_HEIGHT
                    # Adjust intrinsics.
                    calib_current = calib_camera.copy()
                    calib_current[0, 0] *= zoom_x
                    calib_current[0, 2] *= zoom_x
                    calib_current[1, 1] *= zoom_y
                    calib_current[1, 2] *= zoom_y
                    calib_representation = ','.join([str(c) for c in calib_current.flatten()])
                    img = cv2.resize(img, (WIDTH, HEIGHT))
                    big_img[:, wct * WIDTH:(wct + 1) * WIDTH] = img
                    wct += 1
                if not skip_sequence:
                    # save joined sequence, intrinsics, seq-to-frames map (optional:) and mask
                    joined_seq_path = osp.join(OUTPUT_DIR, d, '{}.{}'.format(imgnum, OUTPUT_FILE_EXTENSION))
                    cv2.imwrite(joined_seq_path, big_img)
                    with open(joined_seq_path.replace('.{}'.format(OUTPUT_FILE_EXTENSION), '_cam.txt'), 'w') as f:
                        f.write(calib_representation)
                    with open(osp.join(OUTPUT_DIR, 'seq_to_frames_map.txt'),
                              'a') as f:  # write sequence-to-frame mapping
                        f.write('{},{}\n'.format(osp.join(d, '{}.{}'.format(imgnum, OUTPUT_FILE_EXTENSION)),
                                                 ','.join(frames_paths)))
                    if GENERATE_DUMMY_MASKS:
                        cv2.imwrite(joined_seq_path.replace('.{}'.format(OUTPUT_FILE_EXTENSION), '-fseg.png'),
                                    dummy_mask)
                    ct += 1

    # Split into train/val
    print('split to train/val')
    np.random.seed(8964)
    with open(osp.join(OUTPUT_DIR, 'train.txt'), 'w') as tf:
        with open(osp.join(OUTPUT_DIR, 'val.txt'), 'w') as vf:
            for d in get_list_of_sub_dirs(OUTPUT_DIR):
                if not osp.isdir(osp.join(OUTPUT_DIR, d)):
                    continue
                imfiles = load_dir_files(osp.join(OUTPUT_DIR, d), ext=OUTPUT_FILE_EXTENSION)
                imfiles = [file for file in imfiles if 'seg' not in file and 'cam' not in file]
                frame_ids = [fi.split('.')[0] for fi in imfiles]
                for frame in tqdm(frame_ids):
                    if np.random.random() < VAL_RATIO:
                        vf.write('%s %s\n' % (d, frame))
                    else:
                        tf.write('%s %s\n' % (d, frame))
    print('Done.')


if __name__ == '__main__':
    app.run(main)
