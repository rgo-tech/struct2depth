import pandas as pd
from utils_elad import list_to_text_file
VAL_PATH = '/home/Elad/dev/datasets/Ofek9_processed_no_stops_ang_velocity_0008_gapped_seq_3_5_7/val.txt'
SEQ_TO_FRAMES_MAP_PATH = '/home/Elad/dev/datasets/Ofek9_processed_no_stops_ang_velocity_0008_gapped_seq_3_5_7/seq_to_frames_map.txt'
if __name__ == '__main__':
    # load dfs
    val_df = pd.read_csv(VAL_PATH, sep=' ', header=None, names=['dir', 'seq_num'], dtype=object)
    seq_to_frames_map_df = pd.read_csv(SEQ_TO_FRAMES_MAP_PATH, names=['seq_path', 'frame_1', 'frame_2', 'frame_3'])
    # extract seq num
    val_seqs_list = val_df['seq_num'].tolist()
    seq_to_frames_map_df['seq_num'] = seq_to_frames_map_df['seq_path'].apply(lambda x: x.split('/')[1].split('.')[0])
    # get relevant seqs
    relevant_mask = seq_to_frames_map_df['seq_num'].isin(val_seqs_list)
    seq_to_frames_map_df = seq_to_frames_map_df[relevant_mask]

    seq_to_frames_map_df.drop_duplicates(subset='frame_2', inplace=True)
    unique_val_seqs = ['{} {}'.format(x.split('/')[0], x.split('/')[1].split('.')[0]) for x in seq_to_frames_map_df['seq_path'].tolist()]

    dst_path = VAL_PATH.replace('val.txt', 'val_unique.txt')
    list_to_text_file(dst_path, unique_val_seqs)

    print('Done.')
