import cv2
import numpy as np
import os.path as osp
from tqdm import tqdm
from utils_elad import get_list_of_sub_dirs, load_dir_files

DATASET_DIR = '/home/Elad/dev/datasets/OpenValley'

IS_SEQ = False
if IS_SEQ:
    INPUT_FILE_EXTENSION = 'png'
else:
    INPUT_FILE_EXTENSION = 'pgm'

if __name__ == '__main__':
    mean_sum = 0
    std_sum = 0
    count = 0
    for d in get_list_of_sub_dirs(DATASET_DIR):  # loop over all sub-directories (== sub data-sets)
        curr_subdir = osp.join(DATASET_DIR, d)
        print('preprocess dataset: {}'.format(curr_subdir))
        files = load_dir_files(curr_subdir, ext=INPUT_FILE_EXTENSION)
        files = [file for file in files if '-fseg' not in file]
        for file in tqdm(files):
            img_path = osp.join(curr_subdir, file)
            img = cv2.imread(img_path)
            if IS_SEQ:  # get middle image
                width = int(img.shape[1] / 3)
                img = img[:, width:width * 2, :]
            values = img[:, :, 0].reshape(-1)
            img_mean = np.mean(values)
            img_std = np.std(values)

            mean_sum += img_mean
            std_sum += img_std
            count += 1

    print('dataset mean={}, std={}'.format(mean_sum / count/256.0, std_sum / count/256.0))


    print('Done.')
