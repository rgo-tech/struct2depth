import os.path as osp
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
from scipy.ndimage import binary_dilation
from utils_elad import plot_series_hist

# INPUT_DIR = '/home/Elad/dev/datasets/Ofek9/2020-02-12_075004_Ofek9_3xRTK_310_undistorted'
INPUT_DIR = '/home/Elad/dev/datasets/Hamigdal14/2019-10-17_052024_Hamigdal14_210_undistorted'


def get_cam1_interpolation_func(curr_dir):
    cam_meta_path = osp.join(curr_dir, 'meta', 'cam1.csv')
    cam_df = pd.read_csv(cam_meta_path)
    cam_frame_num = cam_df['frame_num']
    cam_timestamp = cam_df['timestamp_ns']
    return interp1d(cam_timestamp, cam_frame_num)


if __name__ == '__main__':
    robot_meta_path = osp.join(INPUT_DIR, 'meta', 'superdroid_msgs.csv')
    robot_df = pd.read_csv(robot_meta_path)
    robot_df['encoders_diff'] = robot_df['encoder_left'] - robot_df['encoder_right']
    robot_df['encoders_diff_delta_t'] = robot_df['encoders_diff'] - robot_df['encoders_diff'].shift(periods=5)
    robot_df['delta_t'] = robot_df['timestamp_left'] - robot_df['timestamp_left'].shift(periods=5)
    robot_df['angular_velocity'] = (robot_df['encoders_diff_delta_t'] / robot_df['delta_t']).abs()
    plot_series_hist(robot_df['angular_velocity'], 30)

    angular_velocity_cutoff = 0.0002
    robot_df['is_high_ang_velocity'] = robot_df['angular_velocity'] > angular_velocity_cutoff
    binary_structure = np.ones(5)
    robot_df['is_high_ang_velocity'] = binary_dilation(robot_df['is_high_ang_velocity'], binary_structure)
    high_ang_velocity_percent = robot_df['is_high_ang_velocity'].sum() / robot_df.shape[0]
    print("percentage of frames with high angular velocity: {:.2f}%".format(high_ang_velocity_percent * 100))
    # interpolate to frame nums
    inter_time_to_frame = get_cam1_interpolation_func(INPUT_DIR)
    robot_df['frame_num'] = inter_time_to_frame(robot_df['host_timestamp']).round().astype(np.int)
    robot_df.drop_duplicates(subset='frame_num', inplace=True)
    high_ang_velocity_df = robot_df[robot_df['is_high_ang_velocity'] == True]
    print('Done.')
