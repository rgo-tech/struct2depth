import random
import matplotlib.pyplot as plt
import os.path as osp
from utils_elad import get_list_of_sub_dirs, load_dir_files, read_image, crop_image
from gen_data_hamigdal import get_camera_intrinsics, get_valid_movement_frames, filter_files, INPUT_FILE_EXTENSION

SAMPLE_SIZE = 10
INPUT_DIR = '/home/Elad/dev/datasets/Hamigdal14'


if __name__ == '__main__':
    for d in get_list_of_sub_dirs(INPUT_DIR):  # loop over all sub-directories (== sub data-sets)
        curr_subdir = osp.join(INPUT_DIR, d)
        print('preprocess dataset: {}'.format(curr_subdir))
        # get camera intrinsics
        file_calibration = osp.join(curr_subdir, 'meta', 'CameraCalibration.xml')
        calib_camera = get_camera_intrinsics(file_calibration)
        # get all images/frames and filter them
        files = load_dir_files(curr_subdir, ext=INPUT_FILE_EXTENSION)
        files = sorted(files)
        movement_frames, movement_labels = get_valid_movement_frames(curr_subdir)
        files, labels = filter_files(files, movement_frames, movement_labels)
        sampled_files = random.sample(files, SAMPLE_SIZE)
        for sample in sampled_files:
            img_path = osp.join(curr_subdir, sample)
            img = read_image(img_path)
            # img_crop = crop_image(img, y_keep=[0, 280])
            # img_concat = np.concatenate([img, img_crop], axis=0)
            img_line = img.copy()
            img_line[300, :, ...] = 0
            plt.imshow(img_line)
            plt.show()

    print('Done.')
