import os.path as osp
from tqdm import tqdm
from utils_elad import get_list_of_sub_dirs, load_dir_files, list_to_text_file


DATASET_DIR = '/home/Elad/dev/datasets/KITTI_processed_gray/'
OUTPUT_PATH = '/home/Elad/dev/datasets/KITTI_processed_gray/train.txt'
IMAGE_EXTENSION = 'png'

if __name__ == '__main__':
    assert osp.isdir(DATASET_DIR), 'DATASET_DIR does not exist ({})'.format(DATASET_DIR)
    train_list = []
    for subdir in tqdm(get_list_of_sub_dirs(DATASET_DIR)):
        subdir_path = osp.join(DATASET_DIR, subdir)
        for filename in load_dir_files(subdir_path, IMAGE_EXTENSION):
            if '-fseg.' in filename: continue
            train_list.append('{} {}'.format(subdir, filename.split('.{}'.format(IMAGE_EXTENSION))[0]))
    list_to_text_file(OUTPUT_PATH, train_list)
    print('Done.')
