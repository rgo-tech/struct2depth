from utils_elad import get_list_of_all_files
import numpy as np
import cv2
import os.path as osp
from tqdm import tqdm

DATASET_DIR = '/home/Elad/dev/datasets/KITTI_processed_gray/'
SEQ_LENGTH = 3
WIDTH = 416
HEIGHT = 128
if __name__ == '__main__':
    dummy_mask = np.zeros(shape=(HEIGHT, WIDTH * SEQ_LENGTH, 3), dtype=np.uint8)
    all_files = get_list_of_all_files(DATASET_DIR)
    for filename in tqdm(all_files):
        if filename.endswith('.png'):
            cv2.imwrite(filename.replace('.png', '-fseg.png'), dummy_mask)
    print('Done.')
