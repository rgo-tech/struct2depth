run training on KITTI:
- get the imagenet pre-trained resnet-18 ckpt using: https://github.com/dalgu90/resnet-18-tensorflow. Or, try to use the paper's pre-trained model
- generate masks of (0,0,0) to ignore the motion model. https://github.com/tensorflow/models/issues/6173#issuecomment-462080570 or:
- generate objects' instances using maskrcnn. check out this comment for that: https://github.com/tensorflow/models/issues/6392#issuecomment-529786166 and give individual id per object in each sequence (using alignment.py). An object must appear in all sequences.
- generate train.txt for choosing train sequences using get_data_train_text.py

args for running train (train.py):
--checkpoint_dir=/home/Elad/dev/experiments/struct2depth/first_train_KITTI --data_dir=/home/Elad/dev/datasets/KITTI_processed --architecture=resnet --imagenet_ckpt=/home/Elad/dev/pre-trained/resnet18_imagenet/model.ckpt --handle_motion=False --summary_freq=5 --size_constraint_weight=0
