#!/bin/bash
# run command: nohup bash inference_cmd_open_valley.sh&
DATASET_FILE_PATH="/home/Elad/dev/datasets/OpenValley_processed_no_stops_ang_velocity_001_gapped_seq_5/val.txt"
WEIGHTS_PATH="/home/Elad/dev/experiments/struct2depth/OpenValley_train/20200604T141102_no_stops_ang_velocity_001_gapped_seq_5/best/model-392030"
OUTPUT_DIR="/home/Elad/dev/experiments/struct2depth/OpenValley_inference/20200604T141102_no_stops_ang_velocity_001_gapped_seq_5"
SUB_DATASET_NAME="2020-03-31_112527_OpenValley_110_undistorted"
EXPERIMENT_NAME=$(basename $OUTPUT_DIR)

echo "Output directory: $EXPERIMENT_NAME"
if ! [[ -d "$OUTPUT_DIR" ]]
then
    mkdir $OUTPUT_DIR
    chmod -R 777 $OUTPUT_DIR
fi

python /home/Elad/dev/reps/struct2depth/inference_elad.py --output_dir=$OUTPUT_DIR --model_ckpt=$WEIGHTS_PATH --input_list_file=$DATASET_FILE_PATH \
--file_extension=png --depth=True --inference_mode=triplets --dataset_norm=open_valley && \
sudo ffmpeg -framerate 3 -i "$OUTPUT_DIR/$SUB_DATASET_NAME/%*.png" -codec:v libx264 -preset veryslow -pix_fmt yuv420p -crf 10 -an "$OUTPUT_DIR/$SUB_DATASET_NAME/OpenValley_"$EXPERIMENT_NAME"_inference_vid.mp4"
