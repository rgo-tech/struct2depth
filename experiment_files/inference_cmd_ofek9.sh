#!/bin/bash
# run command: nohup bash inference_cmd_ofek9.sh&
DATASET_FILE_PATH="/home/Elad/dev/datasets/Ofek9_processed_no_stops_ang_velocity_0008_gapped_seq_3_5_7/val_unique.txt"
WEIGHTS_PATH="/home/Elad/dev/experiments/struct2depth/Ofek9_train/20200527T113142_no_stops_ang_velocity_0008_gapped_seq_3_5_7_lr_00001_norm/best/model-355230"
OUTPUT_DIR="/home/Elad/dev/experiments/struct2depth/Ofek9_inference/20200527T113142_no_stops_ang_velocity_0008_gapped_seq_3_5_7_lr_00001_norm"
SUB_DATASET_NAME="2020-02-12_075004_Ofek9_3xRTK_310_undistorted"
EXPERIMENT_NAME=$(basename $OUTPUT_DIR)
echo $EXPERIMENT_NAME
if ! [[ -d "$OUTPUT_DIR" ]]
then
    mkdir $OUTPUT_DIR
    chmod -R 777 $OUTPUT_DIR
fi

python /home/Elad/dev/reps/struct2depth/inference_elad.py --output_dir=$OUTPUT_DIR --model_ckpt=$WEIGHTS_PATH --input_list_file=$DATASET_FILE_PATH \
--file_extension=png --depth=True --inference_mode=triplets --dataset_norm=ofek9 && \
sudo ffmpeg -framerate 3 -i "$OUTPUT_DIR/$SUB_DATASET_NAME/%*.png" -codec:v libx264 -preset veryslow -pix_fmt yuv420p -crf 10 -an "$OUTPUT_DIR/$SUB_DATASET_NAME/Ofek9_"$EXPERIMENT_NAME"_inference_vid.mp4"
