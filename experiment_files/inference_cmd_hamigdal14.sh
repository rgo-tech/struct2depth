#!/bin/bash
# run command: nohup bash inference_cmd_hamigdal14.sh&
DATASET_FILE_PATH="/home/Elad/dev/datasets/Hamigdal14_processed_no_stops_ang_velocity_0002_gapped_seq_5_crop_y_300/val.txt"
WEIGHTS_PATH="/home/Elad/dev/experiments/struct2depth/Hamigdal14_train/20200531T191723_Hamigdal14_processed_no_stops_ang_velocity_0002_gapped_seq_5_crop_y_300_norm/best/model-277788"
OUTPUT_DIR="/home/Elad/dev/experiments/struct2depth/Hamigdal14_inference/20200531T191723_processed_no_stops_ang_velocity_0002_gapped_seq_5_crop_y_300_norm"
SUB_DATASET_NAME="2019-10-17_052024_Hamigdal14_210_undistorted"
EXPERIMENT_NAME=$(basename $OUTPUT_DIR)

echo "Output directory: $EXPERIMENT_NAME"
if ! [[ -d "$OUTPUT_DIR" ]]
then
    mkdir $OUTPUT_DIR
    chmod -R 777 $OUTPUT_DIR
fi

python /home/Elad/dev/reps/struct2depth/inference_elad.py --output_dir=$OUTPUT_DIR --model_ckpt=$WEIGHTS_PATH --input_list_file=$DATASET_FILE_PATH \
--file_extension=png --depth=True --inference_mode=triplets --dataset_norm=hamigdal14 && \
sudo ffmpeg -framerate 3 -i "$OUTPUT_DIR/$SUB_DATASET_NAME/%*.png" -codec:v libx264 -preset veryslow -pix_fmt yuv420p -crf 10 -an "$OUTPUT_DIR/$SUB_DATASET_NAME/Hamigdal14_"$EXPERIMENT_NAME"_inference_vid.mp4"
