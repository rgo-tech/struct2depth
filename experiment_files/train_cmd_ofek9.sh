#!/bin/bash
NOW_DATETIME=$(date +"%Y%m%dT%H%M%S")
EXPERIMENT_NAME="no_stops_ang_velocity_0008_gapped_seq_3_5_7_lr_00001_norm"

DATASET_DIR="/home/Elad/dev/datasets/Ofek9_processed_no_stops_ang_velocity_0008_gapped_seq_3_5_7"
EXPERIMENT_DIR="/home/Elad/dev/experiments/struct2depth/Ofek9_train"
PRETRAINED_WEIGHTS="/home/Elad/dev/pre-trained/struct2depth_model_kitti/model-199160"

if ! [[ -d "$EXPERIMENT_DIR" ]]
then
    echo "creating new directory at: $EXPERIMENT_DIR"
    mkdir $EXPERIMENT_DIR
    chmod -R 777 $EXPERIMENT_DIR
fi

EXPERIMENT_DIR="$EXPERIMENT_DIR"/"$NOW_DATETIME"_"$EXPERIMENT_NAME"
echo "creating sub directiry at $EXPERIMENT_DIR"
mkdir $EXPERIMENT_DIR

nohup python ~/dev/reps/struct2depth/train_elad.py --checkpoint_dir=$EXPERIMENT_DIR --data_dir=$DATASET_DIR --pretrained_ckpt=$PRETRAINED_WEIGHTS  --architecture=resnet \
    --handle_motion=False --summary_freq=100 --size_constraint_weight=0 --learning_rate=0.00001 --dataset_norm=ofek9>./train_log_ofek9_"$NOW_DATETIME"_"$EXPERIMENT_NAME".out&

nohup tensorboard --logdir=$EXPERIMENT_DIR --port=9090 --host=0.0.0.0 > ./train_log_tb.out&
