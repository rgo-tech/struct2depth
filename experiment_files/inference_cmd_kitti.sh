#!/bin/bash
#DATASET_DIR="/home/Elad/dev/datasets/KITTI_FULL/kitti-raw-uncompressed/2011_09_26/2011_09_26_drive_0009_sync/image_02/data/"
#OUTPUT_DIR="/home/Elad/dev/experiments/struct2depth/KITTI_inference/pre_trained_model"
#WEIGHTS_PATH="/home/Elad/dev/pre-trained/struct2depth_model_kitti/model-199160"
DATASET_DIR="/home/Elad/dev/datasets/KITTI_processed_gray/2011_09_26_drive_0009_sync_02"
OUTPUT_DIR="/home/Elad/dev/experiments/struct2depth/KITTI_inference/20200516T175043_gray"
WEIGHTS_PATH="/home/Elad/dev/experiments/struct2depth/KITTI_train/20200516T175043_gray/best/model-142878"

if ! [[ -d "$OUTPUT_DIR" ]]
then
    mkdir $OUTPUT_DIR
    chmod -R 777 $OUTPUT_DIR
fi

nohup python ~/dev/reps/struct2depth/inference.py --input_dir=$DATASET_DIR --output_dir=$OUTPUT_DIR --model_ckpt=$WEIGHTS_PATH \
    --file_extension=png --depth=True --inference_mode=triplets > ./inference_log_kitti.out&
